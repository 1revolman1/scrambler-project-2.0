const iplocate = require("node-iplocate");
const ipLocate = (ip) =>
  new Promise(async (resolve, reject) => {
    let results = await iplocate(ip);
    if (results !== null) {
      resolve(results);
    } else {
      reject(results);
    }
  });

module.exports = { ipLocate };
