const cheerio = require("cheerio");
const axios = require("axios");

//------parse data about torrent
const parseData = async function (html) {
  data = [];
  try {
    const $ = cheerio.load(html);
    $(".table tbody .torrent_files").each((index, element) => {
      let last = $(`.table tbody .date-column`)
        .eq(index + index + 1)
        .text();

      data.push({
        name: $(`.table tbody .torrent_files`)
          .eq(index)
          .text()
          .replace(/\s+/g, " "),
        size: $(`.table tbody .size-column`).eq(index).text(),
        lastData: last,
        type:
          $(`.table tbody .category-column`).eq(index).text() &&
          $(`.table tbody .category-column`).eq(index).text().length > 0
            ? $(`.table tbody .category-column`).eq(index).text()
            : "Неизвестно",
        id: $(`.table tbody .torrent_files`)
          .eq(index)
          .find("a")
          .attr("href")
          .replace("/ru/torrent/", "")
          .replace("/", "(++)"),
        // .replaceAll("/", "+"),
        // .split("=")[1],
      });
    });
    return data;
  } catch (msg) {
    return msg;
  }
};

const getTorrents = (ip, type = "ALL") =>
  new Promise(async (resolve, reject) => {
    let response = await axios.get(
      `https://iknowwhatyoudownload.com/ru/peer/?ip=${ip}`
    );
    if (response.status === 200) {
      let torrent_info = await parseData(response.data),
        information;
      if (type === "ALL") {
        information = {
          hasAdultContent: false,
          hasDangerContent: false,
          content: torrent_info,
        };
      } else {
        information = {
          hasAdultContent: false,
          hasDangerContent: false,
          content: torrent_info[0],
        };
      }
      torrent_info.forEach(({ type }) => {
        if (type === "1")
          information = { ...information, hasAdultContent: true };
        if (type === "2")
          information = { ...information, hasDangerContent: true };
      });
      resolve(information);
    } else {
      reject(response.status);
    }
  }).then((data) => data);

// const getTorrents = (ip, type = "ALL") =>
//   new Promise(async (resolve, reject) => {
//     let response = await axios.get(
//       `https://iknowwhatyoudownload.com/ru/peer/?ip=${ip}`
//     );
//     if (response.status === 200) {
//       let torrent_info = await parseData(response.data),
//         information;
//       if (type === "ALL") {
//         information = {
//           hasPornography: false,
//           hasChildPornography: false,
//           content: torrent_info,
//         };
//       } else {
//         information = {
//           hasPornography: false,
//           hasChildPornography: false,
//           content: torrent_info[0],
//         };
//       }
//       torrent_info.forEach(({ type }) => {
//         if (type === "Порно")
//           information = { ...information, hasPornography: true };
//         if (type === "Детское порно")
//           information = { ...information, hasChildPornography: true };
//       });
//       resolve(information);
//     } else {
//       reject(response.status);
//     }
//   }).then((data) => data);

//------parse data about films
const parseFilmData = async function (html) {
  try {
    const $ = cheerio.load(html);
    let data = $(".panel-info .media-body .table tbody tr")
      .map(function (index, element) {
        const first = $(element).find("td").eq(0).text();
        const second =
          ($(element).find("td").eq(1).find("img").eq(1).attr("src") &&
            `${process.env.PRODUCTION_SERVER}/api/proxyimg?url=${$(element)
              .find("td")
              .eq(1)
              .find("img")
              .eq(1)
              .attr("src")}`) ||
          $(element).find("td").eq(1).text();
        return {
          first,
          second,
        };
      })
      .get();
    return {
      photo: `${process.env.PRODUCTION_SERVER}/api/proxyimg?url=${$(
        ".poster-thumb"
      ).attr("src")}`,
      data,
    };
  } catch (msg) {
    return {
      error: true,
    };
  }
};

const getFilms = (id) =>
  new Promise(async (resolve, reject) => {
    let response = await axios.get(
      `https://iknowwhatyoudownload.com/ru/torrent/${id.replace("(++)", "/")}`
    );
    if (response.status === 200) {
      let torrent_info = await parseFilmData(response.data);
      resolve(torrent_info);
    } else {
      reject(response.status);
    }
  }).then((data) => data);
module.exports = { getTorrents, getFilms };
