// const { getRedisClient } = require("../redis");
const session = require("express-session");
// const RedisStore = require("connect-redis")(session);

const sessionFunction = () =>
  session({
    // store: new RedisStore({ client: getRedisClient() }),
    secret: "scrambler-project-2.0 by Revolman",
    resave: false,
    saveUninitialized: false,
  });

module.exports = {
  sessionFunction,
};
