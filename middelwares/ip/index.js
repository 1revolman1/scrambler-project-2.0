function GetIP(req, res, next) {
  if (process.env.TYPE === "production") {
    const ip =
      req.headers["x-forwarded-for"].split(",")[0] ||
      req.headers["x-forwarded-for"] ||
      req.connection.remoteAddress ||
      req.socket.remoteAddress ||
      (req.connection.socket ? req.connection.socket.remoteAddress : null);
    res.locals.ip = ip;

    next();
  } else {
    res.locals.ip = "188.163.96.99";
    // res.locals.ip = "46.133.236.46";
    // res.locals.ip = "188.163.96.86";
    // res.locals.ip = "91.218.97.118";
    // res.locals.ip = "1.1.1.1";
    next();
  }
}

module.exports = {
  GetIP,
};
