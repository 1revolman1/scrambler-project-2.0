const initialData = require("express").Router();
const { getTorrents } = require("../functions/torrent");
const { ipLocate } = require("../functions/ip");

initialData.get("/", async (req, res) => {
  try {
    const { content } = await getTorrents(res.locals.ip, "FIRST");
    const ipdata = await ipLocate(res.locals.ip);
    res.json({ ipdata, torrent: content });
  } catch (msg) {
    res.status(404).json({
      error: true,
      msg,
    });
  }
});

initialData.get("/sidebar", async (req, res) => {
  try {
    const { ip, country } = await ipLocate(res.locals.ip);
    res.json({ ip, country });
  } catch (msg) {
    res.status(404).json({
      error: true,
      msg,
    });
  }
});
module.exports = { initialData };
