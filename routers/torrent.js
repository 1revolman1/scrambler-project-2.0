const torrent = require("express").Router();

const { getTorrents, getFilms } = require("../functions/torrent");
const { ipLocate } = require("../functions/ip");

const getTorrentDataWithIp = async function (ip) {
  return Promise.all([getTorrents(ip), ipLocate(ip)]).then(
    ([torrent, locate]) => {
      return { ...locate, ...torrent };
    }
  );
};

torrent.get("/", async (req, res) => {
  try {
    res.json(await getTorrentDataWithIp(res.locals.ip));
  } catch (msg) {
    res.status(404).json({ error: true, msg });
  }
});
torrent.get("/film/:id", async (req, res) => {
  if (req.params.id.length < 96) {
    res.status(406);
    res.json({ error: true });
  } else {
    try {
      const data = await getFilms(req.params.id);
      if (data && data.data && data.data.length > 0) res.json(data);
      else {
        res.status(404);
        res.json({ error: true });
      }
    } catch (msg) {
      res.status(404).json({ error: true, msg });
    }
  }
});
torrent.get("/:ip", async (req, res) => {
  if (
    new RegExp("^((25[0-5]|(2[0-4]|1[0-9]|[1-9]|)[0-9])(.(?!$)|$)){4}$").test(
      req.params.ip
    )
  ) {
    try {
      res.json(await getTorrentDataWithIp(req.params.ip));
    } catch (msg) {
      res.status(404).json({ error: true, msg });
    }
  } else {
    res.status(404);
    res.json({ error: true });
  }
});

module.exports = { torrent };
