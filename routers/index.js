const { auth } = require("./auth");
const { torrent } = require("./torrent");
const { nickname } = require("./nickname");
const { initialData } = require("./initialdata");
const { proxyimg } = require("./proxyimg");

module.exports = { auth, torrent, nickname, initialData, proxyimg };
