const proxyimg = require("express").Router();
const url = require("url");
const request = require("request");

proxyimg.get("/", async (req, res) => {
  const queryData = url.parse(req.url, true).query;
  if (
    queryData.url &&
    (new RegExp("st.kp.yandex.net").test(queryData.url) ||
      new RegExp("kinopoisk").test(queryData.url))
  ) {
    request({
      url: queryData.url,
    })
      .on("error", function (e) {
        res.end(e);
      })
      .pipe(res);
  } else {
    res.end("no url found");
  }
});
module.exports = { proxyimg };
