//https://gist.github.com/zmts/802dc9c3510d79fd40f9dc38a12bccfc
require("dotenv").config();
const express = require("express");
const app = express();
const path = require("path");

const PORT = process.env.PORT || 8080;
const bodyParser = require("body-parser");
const helmet = require("helmet");
const cookieParser = require("cookie-parser");
const compression = require("compression");
const cors = require("cors");

const { sessionFunction } = require("./middelwares/session");
const { GetIP } = require("./middelwares/ip");

app.use(sessionFunction());
app.use(compression());
app.use(
  cors({
    preflightContinue: true,
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    credentials: true,
    origin: "*",
    allowedHeaders:
      "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept",
  })
);
app.set("trust proxy", true);
app.use(bodyParser.json());
app.use(helmet({ contentSecurityPolicy: false }));
app.use(cookieParser());

app.use(express.static(`${__dirname}/static`));

const { auth, torrent, nickname, initialData, proxyimg } = require("./routers");
// app.use(express.static(path.join(__dirname, "build")));

app.use("/api/authentication", auth);
app.use("/api/torrent", GetIP, torrent);
app.use("/api/nickname", nickname);
app.use("/api/home", GetIP, initialData);
app.use("/api/proxyimg", proxyimg);
// app.use("*", function (req, res) {
//   res.sendFile(path.join(__dirname, "build", "index.html"));
// });

app.listen(PORT, () => {
  console.log(`Server run at port: ${PORT}`);
});
